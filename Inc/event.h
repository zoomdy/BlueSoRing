#ifndef __EVENT_H__
#define __EVENT_H__

#include <stdint.h>

typedef struct _Event
{
  uint32_t tick;
  uint32_t reload;
  void (*func)(void);
  struct _Event *next;
}Event;

typedef struct _EventHead
{
  struct _Event *next;
}EventHead;

void assert_failed(uint8_t* file, uint32_t line);
#define eventAssert(expr) ((expr) ? (void)0 : assert_failed((uint8_t *)__FILE__, __LINE__))
void eventAdd(Event *event);
void eventPoll(void);

#endif /* __EVENT_H__ */
