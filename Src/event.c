#include <stddef.h>
#include "event.h"

static EventHead eventHead = {
    .next=NULL,
};

void eventAdd(Event *event)
{
  Event *it;

  eventAssert(event != NULL);
  eventAssert(event->reload > 0);
  eventAssert(event->func != NULL);

  event->tick = event->reload;
  event->next = NULL;

  if(eventHead.next == NULL)
  {
    eventHead.next = event;
  }
  else
  {
    it = eventHead.next;
    while(it->next != NULL)
    {
      it = it->next;
    }
    it->next = event;
  }
}

void eventPoll(void)
{
  Event *it = eventHead.next;

  while(it != NULL)
  {
    it->tick--;
    if(it->tick == 0)
    {
      it->func();
      it->tick = it->reload;
    }
    it = it->next;
  }
}
