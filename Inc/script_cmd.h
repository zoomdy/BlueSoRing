#ifndef __SCRIPT_CMD__
#define __SCRIPT_CMD__

#include <stdint.h>

#define RS_CMD_MAG   ('m' | ('B' << 8) | ('S' << 16) | ('R' << 24))
#define RS_CMD_NDX   'I' // 设置索引寄存器NDX
#define RS_CMD_RGB   'S' // 设置NDX所指向像数的RGB，并累加NDX寄存器
#define RS_CMD_CLR   '0' // 清除从NDX开始的n个像数
#define RS_CMD_ROR   'R' // 循环右移
#define RS_CMD_ROL   'L' // 循环左移
#define RS_CMD_CNT   'C' // 设置CNT寄存器
#define RS_CMD_DJNZ  'Z' // 减少CNT，如果CNT减少到0则跳转，跳转到当前位置为0，跳转到前一条指令为1，以此类推，只能往前跳转
#define RS_CMD_RJMP  'J' // 无条件跳转，相对当前位置，跳转到当前位置为0，跳转到前一条指令为1，以此类推，只能往前跳转
#define RS_CMD_AJMP  'A' // 绝对跳转，跳转到指定的位置，可以往前也可以往后跳
#define RS_CMD_TCK   'T' // 输出内容，等待n ticks后继续后续的指令
#define RS_CMD_NOP   'N' // 无操作
#define RS_CMD_LOOP  'O' // 循环开始标记
#define RS_CMD_END   'E' // 循环结束，跳转到循环开始处重新开始
#define RS_CMD_BEEP  'B' // 蜂鸣器输出

#define RS_DEF_MAG()                   RS_CMD_MAG
#define RS_DEF_NDX(ndx_16b)            (RS_CMD_NDX | ((ndx_16b) << 16))
#define RS_DEF_RGB(r_8b, g_8b, b_8b)   (RS_CMD_RGB | ((r_8b) << 8) | ((g_8b) << 16) | ((b_8b) << 24))
#define RS_DEF_CLR(num_16b)            (RS_CMD_CLR | ((num_16b) << 16))
#define RS_DEF_ROR(num_16b, r_8b)      (RS_CMD_ROR | ((r_8b) << 8) | ((num_16b) << 16))
#define RS_DEF_ROL(num_16b, r_8b)      (RS_CMD_ROL | ((r_8b) << 8) | ((num_16b) << 16))
#define RS_DEF_CNT(cnt_16b)            (RS_CMD_CNT | ((cnt_16b) << 16))
#define RS_DEF_DJNZ()                  (RS_CMD_DJNZ)
#define RS_DEF_RJMP(label_16b)         (RS_CMD_RJMP| ((label_16b) << 16))
#define RS_DEF_AJMP(label_16b)         (RS_CMD_AJMP| ((label_16b) << 16))
#define RS_DEF_TCK(ticks_16b)          (RS_CMD_TCK | ((ticks_16b) << 16))
#define RS_DEF_NOP(ticks_16b)          (RS_CMD_NOP | ((ticks_16b) << 16))
#define RS_DEF_LOOP()                  (RS_CMD_LOOP)
#define RS_DEF_END()                   (RS_CMD_END)
#define RS_DEF_BEEP(pin_8b, state_8b)  (RS_CMD_BEEP | ((pin_8b) << 16) | ((state_8b) << 24))

#define RS_ARRAY_MAX      (7 * 1024 / 4)

#endif /* __SCRIPT_CMD__ */
