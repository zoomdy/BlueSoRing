/*
 * compile-dump-download.bat ring3
 */

#include "script_cmd.h"

#define DLY_BASE    25
const uint32_t script[RS_ARRAY_MAX] =
{
    RS_DEF_MAG(),

    RS_DEF_NDX(0),
    RS_DEF_CLR(24),

    RS_DEF_NDX(0),
    RS_DEF_RGB(0, 16, 0),
    RS_DEF_RGB(0, 0, 16),
    RS_DEF_NDX(23),
    RS_DEF_RGB(16, 0, 0),
    RS_DEF_NDX(1),
    RS_DEF_TCK(DLY_BASE),

    RS_DEF_LOOP(),

    RS_DEF_CNT(21),
    RS_DEF_ROL(22, 1),
    RS_DEF_TCK(DLY_BASE),
    RS_DEF_DJNZ(),
    RS_DEF_ROL(24, 1),
    RS_DEF_TCK(DLY_BASE),

    RS_DEF_END(),
};
