/**
  ******************************************************************************
  * File Name          : main.c
  * Description        : Main program body
  ******************************************************************************
  *
  * COPYRIGHT(c) 2017 STMicroelectronics
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "stm32f0xx_hal.h"
#include "dma.h"
#include "spi.h"
#include "usart.h"
#include "gpio.h"

/* USER CODE BEGIN Includes */
#include "event.h"
#include "script.h"
/* USER CODE END Includes */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */
/* Private variables ---------------------------------------------------------*/
typedef struct
{
  int (*enter)(void* data);
  void (*exit)(void* data);
  uint32_t (*run)(void* data); // return sleep ticks
  void *data;
}ModeCallback;

static void ledCallback(void);
static Event ledEvent =
{
    .reload = 100,
    .func   = &ledCallback,
};

static void runCallback(void);
static Event runEvent =
{
    .reload = 1,
    .func   = &runCallback,
};

static void buttonCallback(void);
static Event buttonEvent =
{
    .reload = 10,
    .func   = &buttonCallback,
};

#define NVRAM_SECTOR      31
#define NVRAM_BASE        (0x08000000 + NVRAM_SECTOR * 1024)
#define NVRAM_SIZE        4
typedef enum
{
  IDLE = 0,
  ERASE = 1,
  ERASE_BUSY = 2,
  PROGRAM = 3,
  PROGRAM_BUSY = 4,
}NvramState;

static NvramState nvramState = IDLE;
static uint64_t nvramBuffer;
static void nvramCallback(void);
static Event nvramEvent =
{
    .reload = 0xffffffff,
    .func   = &nvramCallback,
};
static void nvramStore(void);
static void nvramLoad(void);

static int ringScriptModeEnter(void* data);
static uint32_t ringScriptModeRun(void* data);
/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
void Error_Handler(void);

/* USER CODE BEGIN PFP */
/* Private function prototypes -----------------------------------------------*/

/* USER CODE END PFP */

/* USER CODE BEGIN 0 */
static void ledCallback(void)
{
  if(HAL_GPIO_ReadPin(LED_GPIO_Port, LED_Pin))
  {
    HAL_GPIO_WritePin(LED_GPIO_Port, LED_Pin, GPIO_PIN_RESET);
  }
  else
  {
    HAL_GPIO_WritePin(LED_GPIO_Port, LED_Pin, GPIO_PIN_SET);
  }
}

static void ledInit(void)
{
  eventAdd(&ledEvent);
}

// Read byte code from 0x08006000
#define scriptUserDefined   ((const uint32_t *)0x08006000)
extern const uint32_t scriptAlarm[];
extern const uint32_t scriptTomato[];
extern const uint32_t scriptFireworks[];
extern const uint32_t scriptSimple[];
extern const uint32_t scriptStatic[];

ModeCallback modeCallback[] =
{
    {
        .enter = &ringScriptModeEnter,
        .run = &ringScriptModeRun,
        .data = (void*)scriptAlarm,
    },
    {
        .enter = &ringScriptModeEnter,
        .run = &ringScriptModeRun,
        .data = (void*)scriptTomato,
    },
    {
        .enter = &ringScriptModeEnter,
        .run = &ringScriptModeRun,
        .data = (void*)scriptFireworks,
    },
    {
        .enter = &ringScriptModeEnter,
        .run = &ringScriptModeRun,
        .data = (void*)scriptSimple,
    },
    {
        .enter = &ringScriptModeEnter,
        .run = &ringScriptModeRun,
        .data = (void*)scriptStatic,
    },
    {
        .enter = &ringScriptModeEnter,
        .run = &ringScriptModeRun,
        .data = (void*)scriptUserDefined,
    },

};

static uint32_t mode = 0;

static void modeChange(void)
{
  ModeCallback *callback;
  int retval = -1;

  callback = &modeCallback[mode];
  if(callback->exit)
  {
    callback->exit(callback->data);
  }

  do
  {
    mode++;
    if(mode >= sizeof(modeCallback) / sizeof(modeCallback[0]))
    {
      mode = 0;
    }

    callback = &modeCallback[mode];
    retval = callback->enter(callback->data);
  }
  while(retval != 0);

  runEvent.tick = 1;
}

static void modeStore(void)
{
  uint32_t *buffer = (uint32_t*)&nvramBuffer;
  buffer[0] = mode;
  nvramStore();
}

static void modeLoad(void)
{
  const uint32_t *buffer = (const uint32_t*)&nvramBuffer;
  nvramLoad();
  mode = buffer[0];
  if(mode >= sizeof(modeCallback) / sizeof(modeCallback[0]))
  {
    mode = 0;
  }
}

static void runCallback(void)
{
  ModeCallback *callback;
  uint32_t ticks;

  callback = &modeCallback[mode];
  do
  {
    ticks = callback->run(callback->data);
  }
  while(ticks == 0);

  runEvent.reload = ticks;
}

static void runInit(void)
{
  ModeCallback *callback;

  modeLoad();
  callback = &modeCallback[mode];
  if(callback->enter)
  {
    callback->enter(callback->data);
  }

  eventAdd(&runEvent);
}

static void buttonCallback(void)
{
  static uint32_t buttonState = 0;
  static uint32_t modeStoreTimeout = 0;

  buttonState <<= 1;
  if(HAL_GPIO_ReadPin(BTN_GPIO_Port, BTN_Pin))
  {
    buttonState |= 1;
  }

  if((buttonState & 0xff) == 0xf0)
  {
    modeChange();
    modeStoreTimeout = 1000 / buttonEvent.reload * 60;
  }

  if(modeStoreTimeout > 0)
  {
    modeStoreTimeout--;
    if(modeStoreTimeout == 0)
    {
      modeStore();
    }
  }
}

static void buttonInit(void)
{
  eventAdd(&buttonEvent);
}

void HAL_FLASH_EndOfOperationCallback(uint32_t ReturnValue)
{
  switch(nvramState)
  {
  case ERASE_BUSY:
    nvramState = PROGRAM;
    break;

  case PROGRAM_BUSY:
    nvramState = IDLE;
    HAL_FLASH_Lock();
    break;

  default:
    break;
  }
}

void HAL_FLASH_OperationErrorCallback(uint32_t ReturnValue)
{
  nvramState = IDLE;
  HAL_FLASH_Lock();
}

static void nvramCallback(void)
{
  FLASH_EraseInitTypeDef eraseInit;

  switch(nvramState)
  {
  case IDLE:
    nvramEvent.reload = 0xffffffff;
    break;

  case ERASE:
    nvramState = ERASE_BUSY;
    eraseInit.TypeErase = FLASH_TYPEERASE_PAGES;
    eraseInit.PageAddress = NVRAM_BASE;
    eraseInit.NbPages = 1;
    if(HAL_FLASH_Unlock() != HAL_OK)
    {
      Error_Handler();
    }
    if(HAL_FLASHEx_Erase_IT(&eraseInit) != HAL_OK)
    {
      Error_Handler();
    }

    break;

  case ERASE_BUSY:
      break;

  case PROGRAM:
    nvramState = PROGRAM_BUSY;
    if(HAL_FLASH_Program_IT(FLASH_PROC_PROGRAMDOUBLEWORD, NVRAM_BASE, nvramBuffer) != HAL_OK)
    {
      Error_Handler();
    }
    break;

  case PROGRAM_BUSY:
      break;

  default:
    break;
  }
}

static void nvramInit(void)
{
  nvramLoad();
  eventAdd(&nvramEvent);
}

static void nvramStore(void)
{
  const uint32_t *nvram = (const uint32_t*)NVRAM_BASE;
  uint32_t *buffer = (uint32_t*)&nvramBuffer;

  if(buffer[0] != nvram[0] || buffer[1] != nvram[1])
  {
    nvramState = ERASE;
    nvramEvent.tick = 1;
    nvramEvent.reload = 1;
  }
}

static void nvramLoad(void)
{
  const uint32_t *nvram = (const uint32_t*)NVRAM_BASE;
  uint32_t *buffer = (uint32_t*)&nvramBuffer;

  buffer[0] = nvram[0];
  buffer[1] = nvram[1];
}

static RingScript ringScript;

static int ringScriptModeEnter(void* data)
{
  int retval;
  retval = ringScriptInit(&ringScript, data);
  return retval;
}

static uint32_t ringScriptModeRun(void* data)
{
  return ringScriptRun(&ringScript);;
}

static uint8_t ringBufferSPI[3 * 8 * RS_PIXEL_MAX];
void ringScriptPortOutput(const uint32_t pixels[])
{
  uint32_t i, j, k;
  uint32_t r, g, b;
  uint32_t grb[3];

  for(i = 0; i < RS_PIXEL_MAX; i++)
  {
    r = (pixels[i] >> 8) & 0xff;
    g = (pixels[i] >> 16) & 0xff;
    b = (pixels[i] >> 24) & 0xff;

    // WS2812 require GRB order.
    grb[0] = g;
    grb[1] = r;
    grb[2] = b;

    for(j = 0; j < 3; j++)
    {
      for(k = 0; k < 8; k++)
      {
        if(grb[j] & (1 << (7 - k)))
        {
          ringBufferSPI[i * 3 * 8 + j * 8 + k] = 0x7C; // 1
        }
        else
        {
          ringBufferSPI[i * 3 * 8 + j * 8 + k] = 0x70; // 0
        }
      }
    }
  }

  HAL_SPI_Transmit_DMA(&hspi1, &ringBufferSPI[0], sizeof(ringBufferSPI));
}

void ringScriptPortBeep(uint32_t pin, uint32_t state)
{
  if(pin == 0)
  {
    if(state)
    {
      HAL_GPIO_WritePin(BEEP_GPIO_Port, BEEP_Pin, GPIO_PIN_RESET);
    }
    else
    {
      HAL_GPIO_WritePin(BEEP_GPIO_Port, BEEP_Pin, GPIO_PIN_SET);
    }
  }
}

static void oneBeep(void)
{
  ringScriptPortBeep(0, 1);
  HAL_Delay(100);
  ringScriptPortBeep(0, 0);
}

/* USER CODE END 0 */

int main(void)
{

  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration----------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* Configure the system clock */
  SystemClock_Config();

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_DMA_Init();
  MX_SPI1_Init();
  MX_USART2_UART_Init();

  /* USER CODE BEGIN 2 */
  ledInit();
  nvramInit();
  runInit();
  buttonInit();
  oneBeep();
  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
  /* USER CODE END WHILE */

  /* USER CODE BEGIN 3 */
    if(SysTick->CTRL & SysTick_CTRL_COUNTFLAG_Msk)
    {
      eventPoll();
    }

  }
  /* USER CODE END 3 */

}

/** System Clock Configuration
*/
void SystemClock_Config(void)
{

  RCC_OscInitTypeDef RCC_OscInitStruct;
  RCC_ClkInitTypeDef RCC_ClkInitStruct;

    /**Initializes the CPU, AHB and APB busses clocks 
    */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = 16;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
  RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL16;
  RCC_OscInitStruct.PLL.PREDIV = RCC_PREDIV_DIV5;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }

    /**Initializes the CPU, AHB and APB busses clocks 
    */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_1) != HAL_OK)
  {
    Error_Handler();
  }

    /**Configure the Systick interrupt time 
    */
  HAL_SYSTICK_Config(HAL_RCC_GetHCLKFreq()/1000);

    /**Configure the Systick 
    */
  HAL_SYSTICK_CLKSourceConfig(SYSTICK_CLKSOURCE_HCLK);

  /* SysTick_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(SysTick_IRQn, 0, 0);
}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @param  None
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler */
  /* User can add his own implementation to report the HAL error return state */
  while(1) 
  {
  }
  /* USER CODE END Error_Handler */ 
}

#ifdef USE_FULL_ASSERT

/**
   * @brief Reports the name of the source file and the source line number
   * where the assert_param error has occurred.
   * @param file: pointer to the source file name
   * @param line: assert_param error line source number
   * @retval None
   */
void assert_failed(uint8_t* file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
    ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */

}

#endif

/**
  * @}
  */ 

/**
  * @}
*/ 

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
