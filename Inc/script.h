#ifndef __RING_SCRIPT_H__
#define __RING_SCRIPT_H__

#include <stdint.h>

#ifndef RS_PIXEL_MAX
#define RS_PIXEL_MAX 24
#endif

typedef struct
{
  uint32_t pc;
  uint32_t pc_djnz;
  uint32_t pc_loop;
  uint32_t cnt;
  uint32_t ndx;
  const uint32_t *scripts;
  uint32_t pixels[RS_PIXEL_MAX];
}RingScript;

int ringScriptInit(RingScript *self, const uint32_t scripts[]);
uint32_t ringScriptRun(RingScript *self);
void ringScriptPortOutput(const uint32_t pixels[]);
void ringScriptPortBeep(uint32_t pin, uint32_t state);

#endif /* __RING_SCRIPT_H__ */
