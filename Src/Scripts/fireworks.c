/*
 * arm-atollic-eabi-gcc.exe -c -o fireworks.o fireworks.c -I ../Inc
 * arm-atollic-eabi-objcopy.exe --dump-section .rodata=fireworks.bin fireworks.o
 * 使用STM32 ST-LINK Utility将test4.bin下载到基地址为0x08004000的flash空间中
 */

#include "script_cmd.h"

#define DELAY    50

	
#define FIREWORKS1(c) \
	RS_DEF_RGB(0, c, c), \
	RS_DEF_RGB(0, 0, 0), \
	RS_DEF_RGB(0, c, 0), \
	RS_DEF_RGB(0, 0, 0), \
	RS_DEF_RGB(c, c, 0), \
	RS_DEF_RGB(0, 0, 0), \
	RS_DEF_RGB(c, 0, c), \
	RS_DEF_RGB(0, 0, 0), \
	RS_DEF_RGB(0, 0, c), \
	RS_DEF_RGB(0, 0, 0), \
	RS_DEF_RGB(c, 0, 0), \
	RS_DEF_RGB(0, 0, 0)
	

#define FIREWORKS2(c) \
	RS_DEF_RGB(c, 0, 0), \
	RS_DEF_RGB(0, 0, 0), \
	RS_DEF_RGB(0, c, c), \
	RS_DEF_RGB(0, 0, 0), \
	RS_DEF_RGB(0, 0, c), \
	RS_DEF_RGB(0, 0, 0), \
	RS_DEF_RGB(c, 0, c), \
	RS_DEF_RGB(0, 0, 0), \
	RS_DEF_RGB(c, c, 0), \
	RS_DEF_RGB(0, 0, 0), \
	RS_DEF_RGB(0, c, 0), \
	RS_DEF_RGB(0, 0, 0)
	
	
#define FIREWORKS3(c) \
	RS_DEF_RGB(c, 0, 0), \
	RS_DEF_RGB(0, 0, 0), \
	RS_DEF_RGB(c, 0, c), \
	RS_DEF_RGB(0, 0, 0), \
	RS_DEF_RGB(0, c, 0), \
	RS_DEF_RGB(0, 0, 0), \
	RS_DEF_RGB(c, c, 0), \
	RS_DEF_RGB(0, 0, 0), \
	RS_DEF_RGB(0, c, c), \
	RS_DEF_RGB(0, 0, 0), \
	RS_DEF_RGB(0, 0, c), \
	RS_DEF_RGB(0, 0, 0)

#define FIREWORKS4(c) \
	RS_DEF_RGB(c, 0, 0), \
	RS_DEF_RGB(0, 0, 0), \
	RS_DEF_RGB(c, 0, c), \
	RS_DEF_RGB(0, 0, 0), \
	RS_DEF_RGB(0, c, 0), \
	RS_DEF_RGB(0, 0, 0), \
	RS_DEF_RGB(c, c, 0), \
	RS_DEF_RGB(0, 0, 0), \
	RS_DEF_RGB(0, c, c), \
	RS_DEF_RGB(0, 0, 0), \
	RS_DEF_RGB(0, 0, c), \
	RS_DEF_RGB(0, 0, 0)
	
const uint32_t scriptFireworks[] =
{
    RS_DEF_MAG(),
	RS_DEF_LOOP(),

#if 1 // chapter 1
    RS_DEF_NDX(0),
    RS_DEF_CLR(24),

	// 从地上升天空
    RS_DEF_NDX(0),
	RS_DEF_RGB(32, 32, 32),
	RS_DEF_NDX(23),
	RS_DEF_RGB(32, 32, 32),
	RS_DEF_TCK(DELAY),
	
	RS_DEF_CNT(11),
	RS_DEF_NDX(0),
	RS_DEF_ROL(12, 1),
	RS_DEF_NDX(12),
	RS_DEF_ROR(12, 1),
	RS_DEF_TCK(DELAY),
	RS_DEF_DJNZ(),
	
	// 在空中爆炸
	RS_DEF_NDX(0),
    RS_DEF_CLR(24),
	RS_DEF_TCK(DELAY),
	
	RS_DEF_NDX(12),
	RS_DEF_RGB(0, 0, 32),
	RS_DEF_TCK(DELAY / 2),
	
	RS_DEF_NDX(11),
	RS_DEF_RGB(32, 0, 0),
	RS_DEF_NDX(13),
	RS_DEF_RGB(32, 0, 0),
	RS_DEF_TCK(DELAY / 2),
	
	RS_DEF_NDX(10),
	RS_DEF_RGB(0, 30, 0),
	RS_DEF_NDX(14),
	RS_DEF_RGB(0, 30, 0),
	RS_DEF_TCK(DELAY / 2),
	
	RS_DEF_NDX(9),
	RS_DEF_RGB(0, 28, 28),
	RS_DEF_NDX(15),
	RS_DEF_RGB(0, 28, 28),
	RS_DEF_TCK(DELAY / 2),
	
	RS_DEF_NDX(8),
	RS_DEF_RGB(26, 26, 0),
	RS_DEF_NDX(16),
	RS_DEF_RGB(26, 26, 0),
	RS_DEF_TCK(DELAY / 2),
	
	RS_DEF_NDX(7),
	RS_DEF_RGB(24, 0, 0),
	RS_DEF_NDX(17),
	RS_DEF_RGB(24, 0, 0),
	RS_DEF_TCK(DELAY / 2),
	
	RS_DEF_NDX(6),
	RS_DEF_RGB(0, 0, 22),
	RS_DEF_NDX(18),
	RS_DEF_RGB(0, 0, 22),
	RS_DEF_TCK(DELAY / 2),
	
	RS_DEF_NDX(5),
	RS_DEF_RGB(20, 0, 20),
	RS_DEF_NDX(19),
	RS_DEF_RGB(20, 0, 20),
	RS_DEF_TCK(DELAY / 2),
	
	RS_DEF_NDX(4),
	RS_DEF_RGB(0, 18, 0),
	RS_DEF_NDX(20),
	RS_DEF_RGB(0, 18, 0),
	RS_DEF_TCK(DELAY / 2),
	
	
	// 天空中一阵乱闪
	RS_DEF_CNT(1),
	RS_DEF_NDX(0),
	FIREWORKS1(16),
	FIREWORKS2(16),
	
	RS_DEF_TCK(DELAY),
	
	RS_DEF_NDX(0),
	FIREWORKS3(16),
	FIREWORKS4(16),
	RS_DEF_ROL(24, 1),
	RS_DEF_TCK(DELAY),
	
	RS_DEF_NDX(0),
	FIREWORKS1(16),
	FIREWORKS3(16),
	RS_DEF_TCK(DELAY),
	
	RS_DEF_NDX(0),
	FIREWORKS2(16),
	FIREWORKS4(16),
	RS_DEF_ROL(24, 1),
	RS_DEF_TCK(DELAY),
	
	RS_DEF_NDX(0),
	FIREWORKS3(16),
	FIREWORKS2(16),
	RS_DEF_TCK(DELAY),
	
	RS_DEF_NDX(0),
	FIREWORKS4(16),
	FIREWORKS1(16),
	RS_DEF_ROL(24, 1),
	RS_DEF_TCK(DELAY),
	
	RS_DEF_DJNZ(),
	
	// 越来越暗
	RS_DEF_CNT(1),
	RS_DEF_NDX(0),
	FIREWORKS1(4),
	FIREWORKS2(4),
	
	RS_DEF_TCK(DELAY),
	
	RS_DEF_NDX(0),
	FIREWORKS3(4),
	FIREWORKS4(4),
	RS_DEF_ROL(24, 1),
	RS_DEF_TCK(DELAY),
	
	RS_DEF_NDX(0),
	FIREWORKS1(4),
	FIREWORKS3(4),
	RS_DEF_TCK(DELAY),
	
	RS_DEF_NDX(0),
	FIREWORKS2(4),
	FIREWORKS4(4),
	RS_DEF_ROL(24, 1),
	RS_DEF_TCK(DELAY),
	
	RS_DEF_NDX(0),
	FIREWORKS3(4),
	FIREWORKS2(4),
	RS_DEF_TCK(DELAY),
	
	RS_DEF_NDX(0),
	FIREWORKS4(4),
	FIREWORKS1(4),
	RS_DEF_ROL(24, 1),
	RS_DEF_TCK(DELAY),
	
	RS_DEF_DJNZ(),
	
	// 更暗
	RS_DEF_CNT(1),
	RS_DEF_NDX(0),
	FIREWORKS1(1),
	FIREWORKS2(1),
	
	RS_DEF_TCK(DELAY),
	
	RS_DEF_NDX(0),
	FIREWORKS3(1),
	FIREWORKS4(1),
	RS_DEF_ROL(24, 1),
	RS_DEF_TCK(DELAY),
	
	RS_DEF_NDX(0),
	FIREWORKS1(1),
	FIREWORKS3(1),
	RS_DEF_TCK(DELAY),
	
	RS_DEF_NDX(0),
	FIREWORKS2(1),
	FIREWORKS4(1),
	RS_DEF_ROL(24, 1),
	RS_DEF_TCK(DELAY),
	
	RS_DEF_NDX(0),
	FIREWORKS3(1),
	FIREWORKS2(1),
	RS_DEF_TCK(DELAY),
	
	RS_DEF_NDX(0),
	FIREWORKS4(1),
	FIREWORKS1(1),
	RS_DEF_ROL(24, 1),
	RS_DEF_TCK(DELAY),
	
	RS_DEF_DJNZ(),
	
	// 停顿
	RS_DEF_NDX(0),
    RS_DEF_CLR(24),
	RS_DEF_TCK(100),
#endif

    RS_DEF_END(),
};
