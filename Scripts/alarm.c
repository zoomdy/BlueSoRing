/*
 * compile-dump-download.bat alarm
 */

#include "script_cmd.h"

#define DELAY    50
#define COLOR    8
#define MINUTES	 20
#define START	 (24 - MINUTES)
  
#define STEP(x) \
  RS_DEF_CNT(60), \
  RS_DEF_NDX(x), \
  RS_DEF_RGB(COLOR, 0, 0), \
  RS_DEF_TCK(500), \
  RS_DEF_NDX(x), \
  RS_DEF_RGB(0, COLOR, 0), \
  RS_DEF_TCK(500), \
  RS_DEF_DJNZ(), \
  RS_DEF_NDX(x), \
  RS_DEF_RGB(COLOR, 0, 0)

  
const uint32_t script[RS_ARRAY_MAX] =
{
  RS_DEF_MAG(),
  
  RS_DEF_NDX(0),
  RS_DEF_CLR(24),
  
  RS_DEF_NDX(START),
  RS_DEF_CNT(MINUTES),
  RS_DEF_RGB(0, COLOR, 0),
  RS_DEF_DJNZ(),
  RS_DEF_TCK(DELAY),

  #if MINUTES > 0
  STEP(START + 0),
  #endif
  #if MINUTES > 1
  STEP(START + 1),
  #endif
  #if MINUTES > 2
  STEP(START + 2),
  #endif
  #if MINUTES > 3
  STEP(START + 3),
  #endif
  #if MINUTES > 4
  STEP(START + 4),
  #endif
  #if MINUTES > 5
  STEP(START + 5),
  #endif
  #if MINUTES > 6
  STEP(START + 6),
  #endif
  #if MINUTES > 7
  STEP(START + 7),
  #endif
  #if MINUTES > 8
  STEP(START + 8),
  #endif
  #if MINUTES > 9
  STEP(START + 9),
  #endif
  #if MINUTES > 10
  STEP(START + 10),
  #endif
  #if MINUTES > 11
  STEP(START + 11),
  #endif
  #if MINUTES > 12
  STEP(START + 12),
  #endif
  #if MINUTES > 13
  STEP(START + 13),
  #endif
  #if MINUTES > 14
  STEP(START + 14),
  #endif
  #if MINUTES > 15
  STEP(START + 15),
  #endif
  #if MINUTES > 16
  STEP(START + 16),
  #endif
  #if MINUTES > 17
  STEP(START + 17),
  #endif
  #if MINUTES > 18
  STEP(START + 18),
  #endif
  #if MINUTES > 19
  STEP(START + 19),
  #endif
  #if MINUTES > 20
  STEP(START + 20),
  #endif
  #if MINUTES > 21
  STEP(START + 21),
  #endif
  #if MINUTES > 22
  STEP(START + 22),
  #endif
  #if MINUTES > 23
  STEP(START + 23),
  #endif
  
  RS_DEF_CNT(5),
  RS_DEF_BEEP(0, 1),
  RS_DEF_TCK(100),
  RS_DEF_BEEP(0, 0),
  RS_DEF_TCK(100),
  RS_DEF_DJNZ(),
  
  RS_DEF_LOOP(),
  RS_DEF_NOP(1000),
  RS_DEF_END(),
};
