/*
 * compile-dump-download.bat alarm
 */

#include "script_cmd.h"

 
const uint32_t script[RS_ARRAY_MAX] =
{
  RS_DEF_MAG(),
  
  RS_DEF_CNT(5),
  RS_DEF_BEEP(0, 1),
  RS_DEF_TCK(100),
  RS_DEF_BEEP(0, 0),
  RS_DEF_TCK(100),
  RS_DEF_DJNZ(),
  
  RS_DEF_LOOP(),
  RS_DEF_TCK(200),
  RS_DEF_END(),
};
