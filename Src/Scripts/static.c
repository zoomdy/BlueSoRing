#include "script_cmd.h"

// 静态点亮，6种颜色
const uint32_t scriptStatic[] =
{
    RS_DEF_MAG(),

    RS_DEF_NDX(0),
    RS_DEF_CLR(24),
    RS_DEF_RGB(8, 0, 0),
    RS_DEF_RGB(8, 0, 0),
    RS_DEF_RGB(8, 0, 0),
    RS_DEF_RGB(8, 0, 0),

    RS_DEF_RGB(0, 8, 0),
    RS_DEF_RGB(0, 8, 0),
    RS_DEF_RGB(0, 8, 0),
    RS_DEF_RGB(0, 8, 0),

    RS_DEF_RGB(0, 0, 8),
    RS_DEF_RGB(0, 0, 8),
    RS_DEF_RGB(0, 0, 8),
    RS_DEF_RGB(0, 0, 8),

    RS_DEF_RGB(0, 8, 8),
    RS_DEF_RGB(0, 8, 8),
    RS_DEF_RGB(0, 8, 8),
    RS_DEF_RGB(0, 8, 8),

    RS_DEF_RGB(8, 0, 8),
    RS_DEF_RGB(8, 0, 8),
    RS_DEF_RGB(8, 0, 8),
    RS_DEF_RGB(8, 0, 8),

    RS_DEF_RGB(8, 8, 8),
    RS_DEF_RGB(8, 8, 8),
    RS_DEF_RGB(8, 8, 8),
    RS_DEF_RGB(8, 8, 8),

    RS_DEF_TCK(100),
    RS_DEF_AJMP(1),
};
