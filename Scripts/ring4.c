/*
 * compile-dump-download.bat ring4
 */

#include "script_cmd.h"

#define DLY_BASE    20
const uint32_t script[RS_ARRAY_MAX] =
{
    RS_DEF_MAG(),

    RS_DEF_NDX(0),
    RS_DEF_CLR(24),

    RS_DEF_NDX(0),
    RS_DEF_RGB(0, 0, 16),
    RS_DEF_NDX(23),
    RS_DEF_RGB(0, 0, 16),
    RS_DEF_TCK(DLY_BASE),

    RS_DEF_CNT(11),
    RS_DEF_NDX(0),
    RS_DEF_ROL(12, 1),
    RS_DEF_NDX(12),
    RS_DEF_ROR(12, 1),
    RS_DEF_TCK(DLY_BASE),
    RS_DEF_DJNZ(),
	
    RS_DEF_CNT(11),
    RS_DEF_NDX(0),
    RS_DEF_ROR(12, 1),
    RS_DEF_NDX(12),
    RS_DEF_ROL(12, 1),
    RS_DEF_TCK(DLY_BASE),
    RS_DEF_DJNZ(),

    RS_DEF_AJMP(1),
};
