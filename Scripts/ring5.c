/*
 * compile-dump-download.bat ring5
 */

#include "script_cmd.h"

#define DELAY    	50
#define COLOR		8

#define STEP_R(x) \
	RS_DEF_NDX(0), \
	RS_DEF_RGB(COLOR, 0, 0), \
	RS_DEF_TCK(DELAY), \
	RS_DEF_CNT(x), \
	RS_DEF_NDX(0), \
	RS_DEF_ROL(x + 1, 1), \
	RS_DEF_TCK(DELAY), \
	RS_DEF_DJNZ()

#define STEP_G(x) \
	RS_DEF_NDX(0), \
	RS_DEF_RGB(0, COLOR, 0), \
	RS_DEF_TCK(DELAY), \
	RS_DEF_CNT(x), \
	RS_DEF_NDX(0), \
	RS_DEF_ROL(x + 1, 1), \
	RS_DEF_TCK(DELAY), \
	RS_DEF_DJNZ()

#define STEP_B(x) \
	RS_DEF_NDX(0), \
	RS_DEF_RGB(0, 0, COLOR), \
	RS_DEF_TCK(DELAY), \
	RS_DEF_CNT(x), \
	RS_DEF_NDX(0), \
	RS_DEF_ROL(x + 1, 1), \
	RS_DEF_TCK(DELAY), \
	RS_DEF_DJNZ()
	
	
#define STEP_RG(x) \
	RS_DEF_NDX(0), \
	RS_DEF_RGB(COLOR, COLOR, 0), \
	RS_DEF_TCK(DELAY), \
	RS_DEF_CNT(x), \
	RS_DEF_NDX(0), \
	RS_DEF_ROL(x + 1, 1), \
	RS_DEF_TCK(DELAY), \
	RS_DEF_DJNZ()

#define STEP_GB(x) \
	RS_DEF_NDX(0), \
	RS_DEF_RGB(0, COLOR, COLOR), \
	RS_DEF_TCK(DELAY), \
	RS_DEF_CNT(x), \
	RS_DEF_NDX(0), \
	RS_DEF_ROL(x + 1, 1), \
	RS_DEF_TCK(DELAY), \
	RS_DEF_DJNZ()

#define STEP_RB(x) \
	RS_DEF_NDX(0), \
	RS_DEF_RGB(COLOR, 0, COLOR), \
	RS_DEF_TCK(DELAY), \
	RS_DEF_CNT(x), \
	RS_DEF_NDX(0), \
	RS_DEF_ROL(x + 1, 1), \
	RS_DEF_TCK(DELAY), \
	RS_DEF_DJNZ()
const uint32_t script[RS_ARRAY_MAX] =
{
  RS_DEF_MAG(),
	
  RS_DEF_LOOP(),

	RS_DEF_NDX(0),
	RS_DEF_CLR(24),
	
	// 1
	STEP_R(23),
	STEP_G(22),
	STEP_B(21),
	STEP_RG(20),
	STEP_GB(19),
	STEP_RB(18),
	// 7
	STEP_R(17),
	STEP_G(16),
	STEP_B(15),
	STEP_RG(14),
	STEP_GB(13),
	STEP_RB(12),
	// 13
	STEP_R(11),
	STEP_G(10),
	STEP_B(9),
	STEP_RG(8),
	STEP_GB(7),
	STEP_RB(6),
	// 19
	STEP_R(5),
	STEP_G(4),
	STEP_B(3),
	STEP_RG(2),
	STEP_GB(1),
	//STEP_RB(0),
	RS_DEF_NDX(0),
	RS_DEF_RGB(COLOR, 0, COLOR),
	RS_DEF_TCK(DELAY),
	
	
	RS_DEF_CNT(96),
	RS_DEF_NDX(0),
	RS_DEF_ROR(24, 1),
	RS_DEF_TCK(DELAY),
	RS_DEF_DJNZ(),
	
	RS_DEF_CNT(6),
	RS_DEF_NDX(0),
	RS_DEF_CLR(1),
	RS_DEF_NDX(6),
	RS_DEF_CLR(1),
	RS_DEF_NDX(12),
	RS_DEF_CLR(1),
	RS_DEF_NDX(18),
	RS_DEF_CLR(1),
	RS_DEF_NDX(0),
	RS_DEF_ROL(24, 1),
	RS_DEF_TCK(DELAY * 10),
	RS_DEF_DJNZ(),
	
	RS_DEF_END(),
};
