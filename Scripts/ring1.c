/*
 * compile-dump-download.bat ring1
 */

#include "script_cmd.h"

#define DLY_BASE    50
const uint32_t script[RS_ARRAY_MAX] =
{
    RS_DEF_MAG(),

    // Clock-wise

    RS_DEF_NDX(0),
    RS_DEF_CLR(24),
    RS_DEF_NDX(8),
    RS_DEF_RGB(0, 0, 8),
    RS_DEF_NDX(12),
    RS_DEF_RGB(0, 0, 8),
    RS_DEF_TCK(DLY_BASE * 3 / 2 * 3 / 2 * 3 / 2),

    RS_DEF_NDX(0),
    RS_DEF_CLR(24),
    RS_DEF_NDX(9),
    RS_DEF_RGB(0, 0, 8),
    RS_DEF_NDX(12),
    RS_DEF_RGB(0, 0, 8),
    RS_DEF_TCK(DLY_BASE * 3 / 2 * 3 / 2),

    RS_DEF_NDX(0),
    RS_DEF_CLR(24),
    RS_DEF_NDX(10),
    RS_DEF_RGB(0, 0, 8),
    RS_DEF_NDX(12),
    RS_DEF_RGB(0, 0, 8),
    RS_DEF_TCK(DLY_BASE * 3 / 2),

    RS_DEF_NDX(0),
    RS_DEF_CLR(24),
    RS_DEF_NDX(11),
    RS_DEF_RGB(0, 0, 8),
    RS_DEF_NDX(12),
    RS_DEF_RGB(0, 0, 8),
    RS_DEF_TCK(DLY_BASE),

    RS_DEF_NDX(0),
    RS_DEF_CLR(24),
    RS_DEF_NDX(12),
    RS_DEF_RGB(0, 0, 8),
    RS_DEF_NDX(13),
    RS_DEF_RGB(0, 0, 8),
    RS_DEF_TCK(DLY_BASE),

    RS_DEF_NDX(0),
    RS_DEF_CLR(24),
    RS_DEF_NDX(12),
    RS_DEF_RGB(0, 0, 8),
    RS_DEF_NDX(14),
    RS_DEF_RGB(0, 0, 8),
    RS_DEF_TCK(DLY_BASE * 3 / 2),

    RS_DEF_NDX(0),
    RS_DEF_CLR(24),
    RS_DEF_NDX(12),
    RS_DEF_RGB(0, 0, 8),
    RS_DEF_NDX(15),
    RS_DEF_RGB(0, 0, 8),
    RS_DEF_TCK(DLY_BASE * 3 / 2 * 3 / 2),

    RS_DEF_NDX(0),
    RS_DEF_CLR(24),
    RS_DEF_NDX(12),
    RS_DEF_RGB(0, 0, 8),
    RS_DEF_NDX(16),
    RS_DEF_RGB(0, 0, 8),
    RS_DEF_TCK(DLY_BASE * 3 / 2 * 3 / 2 * 3 / 2),

    // Counter Clock-wise

    RS_DEF_NDX(0),
    RS_DEF_CLR(24),
    RS_DEF_NDX(12),
    RS_DEF_RGB(0, 0, 8),
    RS_DEF_NDX(15),
    RS_DEF_RGB(0, 0, 8),
    RS_DEF_TCK(DLY_BASE * 3 / 2 * 3 / 2),

    RS_DEF_NDX(0),
    RS_DEF_CLR(24),
    RS_DEF_NDX(12),
    RS_DEF_RGB(0, 0, 8),
    RS_DEF_NDX(14),
    RS_DEF_RGB(0, 0, 8),
    RS_DEF_TCK(DLY_BASE * 3 / 2),

    RS_DEF_NDX(0),
    RS_DEF_CLR(24),
    RS_DEF_NDX(12),
    RS_DEF_RGB(0, 0, 8),
    RS_DEF_NDX(13),
    RS_DEF_RGB(0, 0, 8),
    RS_DEF_TCK(DLY_BASE),

    RS_DEF_NDX(0),
    RS_DEF_CLR(24),
    RS_DEF_NDX(11),
    RS_DEF_RGB(0, 0, 8),
    RS_DEF_NDX(12),
    RS_DEF_RGB(0, 0, 8),
    RS_DEF_TCK(DLY_BASE),

    RS_DEF_NDX(0),
    RS_DEF_CLR(24),
    RS_DEF_NDX(10),
    RS_DEF_RGB(0, 0, 8),
    RS_DEF_NDX(12),
    RS_DEF_RGB(0, 0, 8),
    RS_DEF_TCK(DLY_BASE * 3 / 2 * 3 / 2),

    RS_DEF_NDX(0),
    RS_DEF_CLR(24),
    RS_DEF_NDX(9),
    RS_DEF_RGB(0, 0, 8),
    RS_DEF_NDX(12),
    RS_DEF_RGB(0, 0, 8),
    RS_DEF_TCK(DLY_BASE * 3 / 2 * 3 / 2 * 3 / 2),

    RS_DEF_AJMP(1),
};
