/*
 * compile-dump-download.bat ring6
 */

#include "script_cmd.h"

#define DELAY     25
#define COLOR     16

#define STEP(x) \
  RS_DEF_NDX(x), \
  RS_DEF_CNT(12), \
  RS_DEF_ROL(13, 1), \
  RS_DEF_TCK(DELAY), \
  RS_DEF_DJNZ()

const uint32_t script[RS_ARRAY_MAX] =
{
  RS_DEF_MAG(),

  RS_DEF_NDX(0),
  RS_DEF_CLR(24),
  RS_DEF_NDX(0),

  RS_DEF_CNT(12),
  RS_DEF_RGB(0, 0, COLOR),
  RS_DEF_DJNZ(),
  RS_DEF_TCK(DELAY),

  RS_DEF_LOOP(),

  STEP(11),
  STEP(10),
  STEP(9),
  STEP(8),
  STEP(7),
  STEP(6),

  STEP(5),
  STEP(4),
  STEP(3),
  STEP(2),
  STEP(1),
  STEP(0),

  STEP(23),
  STEP(22),
  STEP(21),
  STEP(20),
  STEP(19),
  STEP(18),

  STEP(17),
  STEP(16),
  STEP(15),
  STEP(14),
  STEP(13),
  STEP(12),

  RS_DEF_END(),
};

