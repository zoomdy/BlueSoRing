#include "script_cmd.h"

// 4种颜色顺时针旋转，然后逆时针旋转，永久循环
const uint32_t scriptSimple[] =
{
    RS_DEF_MAG(),

    RS_DEF_NDX(0),
    RS_DEF_CLR(24),
    RS_DEF_RGB(8, 0, 0),
    RS_DEF_RGB(8, 0, 0),
    RS_DEF_RGB(8, 0, 0),

    RS_DEF_NDX(6),
    RS_DEF_RGB(0, 8, 0),
    RS_DEF_RGB(0, 8, 0),
    RS_DEF_RGB(0, 8, 0),

    RS_DEF_NDX(12),
    RS_DEF_RGB(0, 0, 8),
    RS_DEF_RGB(0, 0, 8),
    RS_DEF_RGB(0, 0, 8),

    RS_DEF_NDX(18),
    RS_DEF_RGB(8, 8, 0),
    RS_DEF_RGB(8, 8, 0),
    RS_DEF_RGB(8, 8, 0),

    RS_DEF_CNT(24 * 3),
    RS_DEF_NDX(0),
    RS_DEF_ROR(24, 1),
    RS_DEF_TCK(100),
    RS_DEF_DJNZ(),
    RS_DEF_CNT(24 * 3),
    RS_DEF_NDX(0),
    RS_DEF_ROL(24, 1),
    RS_DEF_TCK(100),
    RS_DEF_DJNZ(),
    RS_DEF_RJMP(10),
};
