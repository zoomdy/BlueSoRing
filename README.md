#BlueSoRing
蓝瘦彩环，NUCLEO-F042K6开发板驱动WS2812彩色LED灯环。这里仅包含固件，所有硬件均是成品，网上直接可以购买到。

###运行效果
![运行效果](http://git.oschina.net/uploads/images/2017/0122/181502_6f1685c1_428615.jpeg "运行效果")

###接线图
![接线图](http://git.oschina.net/uploads/images/2017/0122/181516_a6775fd0_428615.jpeg "接线图")

###原材料
![原材料](http://git.oschina.net/uploads/images/2017/0122/181526_4bf89aba_428615.jpeg "原材料")

###关键字
NUCLEO-F042K6、STM32F042K6、Cortex-M0、WS2812、SPI、DMA
