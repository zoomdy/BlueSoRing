set stlink="C:\Program Files (x86)\STMicroelectronics\STM32 ST-LINK Utility\ST-LINK Utility\ST-LINK_CLI.exe"
set gcc=arm-atollic-eabi-gcc.exe
set objcopy=arm-atollic-eabi-objcopy.exe

%gcc% -c -o %1.o %1.c -I ../Inc && %objcopy% --dump-section .rodata=%1.bin %1.o && %stlink% -SE 24 30 && %stlink% -P %1.bin 0x08006000 && %stlink% -Rst
