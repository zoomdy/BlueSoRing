#include "script_cmd.h"

#define DELAY    		50
#define COLOR    		8
#define WORK_MINUTES	 24
#define WORK_START	 	(24 - WORK_MINUTES)
#define REST_MINUTES	5
#define REST_START      (24 - REST_MINUTES)
  
#define STEP(x) \
  RS_DEF_CNT(60), \
  RS_DEF_NDX(x), \
  RS_DEF_RGB(COLOR, 0, 0), \
  RS_DEF_TCK(500), \
  RS_DEF_NDX(x), \
  RS_DEF_RGB(0, COLOR, 0), \
  RS_DEF_TCK(500), \
  RS_DEF_DJNZ(), \
  RS_DEF_NDX(x), \
  RS_DEF_RGB(COLOR, 0, 0)
  
const uint32_t scriptTomato[] =
{
  RS_DEF_MAG(),
  RS_DEF_LOOP(),
  
  // work
  RS_DEF_NDX(0),
  RS_DEF_CLR(24),
  
  RS_DEF_NDX(WORK_START),
  RS_DEF_CNT(WORK_MINUTES),
  RS_DEF_RGB(0, COLOR, 0),
  RS_DEF_DJNZ(),
  RS_DEF_TCK(DELAY),

  #if WORK_MINUTES > 0
  STEP(WORK_START + 0),
  #endif
  #if WORK_MINUTES > 1
  STEP(WORK_START + 1),
  #endif
  #if WORK_MINUTES > 2
  STEP(WORK_START + 2),
  #endif
  #if WORK_MINUTES > 3
  STEP(WORK_START + 3),
  #endif
  #if WORK_MINUTES > 4
  STEP(WORK_START + 4),
  #endif
  #if WORK_MINUTES > 5
  STEP(WORK_START + 5),
  #endif
  #if WORK_MINUTES > 6
  STEP(WORK_START + 6),
  #endif
  #if WORK_MINUTES > 7
  STEP(WORK_START + 7),
  #endif
  #if WORK_MINUTES > 8
  STEP(WORK_START + 8),
  #endif
  #if WORK_MINUTES > 9
  STEP(WORK_START + 9),
  #endif
  #if WORK_MINUTES > 10
  STEP(WORK_START + 10),
  #endif
  #if WORK_MINUTES > 11
  STEP(WORK_START + 11),
  #endif
  #if WORK_MINUTES > 12
  STEP(WORK_START + 12),
  #endif
  #if WORK_MINUTES > 13
  STEP(WORK_START + 13),
  #endif
  #if WORK_MINUTES > 14
  STEP(WORK_START + 14),
  #endif
  #if WORK_MINUTES > 15
  STEP(WORK_START + 15),
  #endif
  #if WORK_MINUTES > 16
  STEP(WORK_START + 16),
  #endif
  #if WORK_MINUTES > 17
  STEP(WORK_START + 17),
  #endif
  #if WORK_MINUTES > 18
  STEP(WORK_START + 18),
  #endif
  #if WORK_MINUTES > 19
  STEP(WORK_START + 19),
  #endif
  #if WORK_MINUTES > 20
  STEP(WORK_START + 20),
  #endif
  #if WORK_MINUTES > 21
  STEP(WORK_START + 21),
  #endif
  #if WORK_MINUTES > 22
  STEP(WORK_START + 22),
  #endif
  #if WORK_MINUTES > 23
  STEP(WORK_START + 23),
  #endif
  
  RS_DEF_CNT(5),
  RS_DEF_BEEP(0, 1),
  RS_DEF_TCK(100),
  RS_DEF_BEEP(0, 0),
  RS_DEF_TCK(100),
  RS_DEF_DJNZ(),
  
  // rest
  RS_DEF_NDX(0),
  RS_DEF_CLR(24),
  
  RS_DEF_NDX(REST_START),
  RS_DEF_CNT(REST_MINUTES),
  RS_DEF_RGB(0, COLOR, 0),
  RS_DEF_DJNZ(),
  RS_DEF_TCK(DELAY),
  
  #if REST_MINUTES > 0
  STEP(REST_START + 0),
  #endif
  #if REST_MINUTES > 1
  STEP(REST_START + 1),
  #endif
  #if REST_MINUTES > 2
  STEP(REST_START + 2),
  #endif
  #if REST_MINUTES > 3
  STEP(REST_START + 3),
  #endif
  #if REST_MINUTES > 4
  STEP(REST_START + 4),
  #endif
  
  RS_DEF_CNT(5),
  RS_DEF_BEEP(0, 1),
  RS_DEF_TCK(100),
  RS_DEF_BEEP(0, 0),
  RS_DEF_TCK(100),
  RS_DEF_DJNZ(),
  
  RS_DEF_END(),
};
